import './stopwatch.css';
import '../fonts/DS-DIGI.TTF';
function Display (props){
    const pad = function(n, size) {
        var s = String(n);
        while (s.length < (size || 2)) {s = "0" + s;}
        return s;
    }
    const date = new Date(props.children);
    const del = date.getMilliseconds() <= 500 ? ":" : ".";
    return <span className="font-face-dg"> {pad(date.getMinutes(),2) + " " + del + " " + pad(date.getSeconds(),2) + " " + del + " " + pad(Math.floor(date.getMilliseconds() / 10),2)} </span>
}
export default Display