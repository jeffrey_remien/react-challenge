

import { useState, useEffect} from "react";
import Button from './Button';
import Display from "./Display";

function StopWatch (props) {
    /**
     * elevated state to control all parameters from stopwatch component
     * state is keeping track of current timer value and last update time. Time is calculated with actual time passed since last update
     * this is to make sure the clock runs accurately despite being updated in half second steps sice setInteval is fast but not perfectly accurate 
    */
    const [lastTimerUpdate, setLastTimerUpdate ] = useState(0);  //last time the timer was updated
    const [timerCurrent, setTimerCurrent] = useState(0);    //current timer value
    const [running, setRunning] = useState(false);  //state of the stopwatch

    const startTimer = () => {
        setRunning(true);
    }

    const stopTimer = () => {
        setRunning(false);
    }

    const resetTimer = () => {

        setTimerCurrent(0);
    }

    useEffect(() => {
        let interval = null;  
        if (running) {
            setLastTimerUpdate(Date.now());//this ensures the amount by which the timer will be increased is zero'd out when starting
            interval = setInterval(() => {
                const mil = Date.now(); //making sure we're not losing some mils between setting the current timer and setting the update timestamp
                setTimerCurrent(timerCurrent + mil - lastTimerUpdate); //calculating the new timer value by adding the difference of the current time and last update time
                setLastTimerUpdate(mil);
            }, 500);
        } 
        
        else {
            clearInterval(interval);
        }
        
        return () => {
            clearInterval(interval);
        };
    }, [running, lastTimerUpdate, timerCurrent]);


    const startButton = running ? <Button onClick={stopTimer}>stop</Button> : <Button onClick={startTimer}>start</Button>;
    return(
        <div>
                <Display>{timerCurrent}</Display>
                {startButton}
                <Button onClick= {resetTimer}>reset</Button>
        </div>
    )
}

export default StopWatch